;;; Copyright © 2023 gemmaro <gemmaro.dev@gmail.com>
;;;
;;; This file is part of the Guix-jp channel.
;;;
;;; The Guix-jp channel is free software: you can redistribute it
;;; and/or modify it under the terms of the GNU General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (guix-jp packages emacs-xyz)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (guix build-system emacs)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public emacs-ddskk-posframe
  (let ((commit "299493dd951e5a0b43b8213321e3dc0bac10f762")
        (revision "0"))
    (package
      (name "emacs-ddskk-posframe")
      (version (git-version "1.0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/conao3/ddskk-posframe.el")
               (commit commit)))
         (sha256
          (base32 "1rsy0wjncxzjhis8jrxpxjh9l9pw0bngg1sb4dj5qvhsgszhawpn"))))
      (build-system emacs-build-system)
      (arguments
       (list
        #:tests? #t
        #:test-command #~(list "buttercup" "-L" ".")))
      (native-inputs (list emacs-buttercup))
      (propagated-inputs (list emacs-posframe emacs-ddskk))
      (home-page "https://github.com/conao3/ddskk-posframe.el")
      (synopsis "Show Henkan tooltip for ddskk via posframe")
      (description
       "@samp{ddskk-posframe.el} provide Henkan tooltip for ddskk via
posframe, which can pop up a frame at point.  Use
@code{ddskk-posframe-mode} to enable this minor mode.")
      (license license:agpl3))))
