;;; Copyright © 2022 ROCKTAKEY <rocktakey@gmail.com>
;;;
;;; This file is part of the Guix-jp channel.
;;;
;;; The Guix-jp channel is free software: you can redistribute it
;;; and/or modify it under the terms of the GNU General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (guix-jp packages fonts)
  #:use-module (guix packages)
  #:use-module (guix licenses)
  #:use-module (guix download)
  #:use-module (guix build-system font))

(define-public font-cica
  (package
    (name "font-cica")
    (version "5.0.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/miiton/Cica/releases/download/"
             "v"
             version
             "/Cica_v"
             version
             ".zip"))
       (sha256
        (base32 "0vshn2cd70mnbavsw9cbagcasa95wiv9qdj4wkzxn7gxygqvrlfb"))))
    (build-system font-build-system)
    (home-page "https://github.com/miiton/Cica")
    (synopsis "Japanese monospaced font for programming")
    (description
     "Cica is a Japanese monospaced font for programming.
Hack + DejaVu Sans Mono is used for ASCII, and Rounded Mgen+ for the other.
In addition, Nerd Fonts, Noto Emoji, Icons for Devs, and some adjustment forked from
the Ricty generator are converted and adjusted.")
    (license silofl1.1)))
