[日本語](README.ja.md)/English

# Guix-jp Official channel
This is the [Guix](https://guix.gnu.org/) channel provided by Guix-jp.
It mainly provides packages about Japanese.
See [here](http://guix-jp.gitlab.io/) if you would like to know about Guix-jp.

# Getting Started
With guix installed, add to `~/.config/guix/channels.scm`:
```scheme
(cons* (channel
        (name 'guix-jp)
        (url "https://gitlab.com/guix-jp/channel")
        (branch "main"))
       ;; You can add other channels here
       ;; (channel
       ;;  (name 'another-channel)
       ;;  (url "https://example.com/another/channel"))
       %default-channels)
```

Next, run:
```shell
guix pull
```

Then, you can use all packages provided by this channel.
Install the packages with the command `guix install`.

# Contribution
Welcome to your contribution. Any question, improvement, and package request is accepted
on [Issues](https://gitlab.com/guix-jp/channel/-/issues) or [Guix-jp community](http://guix-jp.gitlab.io/).
Also any merge requests are welcome.

# License
This repository is published under [GNU General Public License v3.0](LICENSE).
